class Coche:
    def __init__ (self, color, marca, modelo,matricula, velocidad):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad
    def aceleracion (self):
        aceleracion = self.velocidad*2
        return aceleracion
    def frenar (self):
        frenar = self.velocidad/2
        return frenar
    def __str__(self):
        return ("La aceleración del coche es {aceleracion}m/s y el freno es {freno}m/s".format(aceleracion= self.aceleracion(), freno = self.frenar()))
coche01 = Coche("blanco", "fiat", "500", "9876zyx", 30)
print(coche01)